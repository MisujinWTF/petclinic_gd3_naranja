package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.samples.petclinic.owner.Owner;
import org.springframework.samples.petclinic.owner.OwnerRepository;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTest {

	@Autowired
	private VisitRepository visitRepository;
	@Autowired
	private PetRepository petRepository;
	@Autowired
	private VetRepository vetRepository;
	@Autowired
	private OwnerRepository ownerRepository;
	
	private Visit visit;
	private Pet pet;
	private Vet vet;
	private Owner owner;
	
	@Before
	public void init() {
		
		if(this.vet == null) {
			vet = new Vet();
			vet.setFirstName("Juan");
			vet.setLastName("Mauricio");
			vet.setHomeVisits(true);
			
			Collection <Specialty> vetSpec = this.vetRepository.findSpecialties();
			
			if (!vetSpec.isEmpty()) {				
				for (Specialty spec : vetSpec) {
					vet.addSpecialty(spec);
				}				
			}		
			
			this.vetRepository.save(vet);
		}
		
		if(this.pet == null) {
			pet = new Pet();
			//pet.setId(9999);
			pet.setBirthDate(LocalDate.of(2018, Month.DECEMBER, 25));
			pet.setComments("Revisión de las patas");
			pet.setName("Romerale");
			PetType petType = new PetType();
			petType.setId(3);
			pet.setType(petType);
			pet.setWeight(100);
		}
		
		if(this.owner == null) {
			owner = new Owner();
			owner.setAddress("Avenida Europa");
			owner.setCity("Cáceres");
			owner.setFirstName("Paco");
			owner.setLastName("Martínez");
			owner.setTelephone("648393002");
			owner.addPet(pet);
			this.ownerRepository.save(owner);
			this.petRepository.save(pet);
		}
		
		if (this.visit==null) {
			visit = new Visit ();
			visit.setDate(LocalDate.of(2012, Month.JUNE, 25));
			visit.setDescription("Revisión de las patas");
			visit.setPetId(pet.getId());
			visit.setVet(vet);

			this.visitRepository.save(visit);
		}
	}
	
	
	@Test
	public void testFindById() {
		
		Visit visitFindById = this.visitRepository.findById(visit.getId());
		
		assertNotNull(visitFindById.getDate());
		assertEquals(visitFindById.getDate(), visit.getDate());	
		
		assertNotNull(visitFindById.getDescription());
		assertEquals(visitFindById.getDescription(), visit.getDescription());	
		
		assertEquals(visitFindById.getId(), visit.getId());
	}
	
	@After
	public void finish() {
		this.visit=null;
	}

}
